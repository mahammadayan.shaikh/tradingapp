package com.repository;

import com.excepetions.StockDoesNotExistException;
import com.excepetions.StockNotAvailableException;
import com.models.Fruit;
import com.strategy.LIFOTradingStrategy;
import com.strategy.TradingStrategy;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

public class TradingRepositoryImplementation implements TradingRepository {
    // Map that stores Queue of particular fruits
    private Map<String, Queue> fruitStock;

    // Variable that stores total profit earned by farmer
    private int profit = 0;

    // Strategy object to manage Trading Operations
    private TradingStrategy strategy;

    public TradingRepositoryImplementation() {
        fruitStock = new HashMap<>();
        strategy = new LIFOTradingStrategy();
    }


    @Override
    public void buy(String name, int price, int quantity) {
        // Create fruit object and pass it to buy method to strategy.buy method
        strategy.buy(new Fruit(name, price, quantity), fruitStock);
    }

    @Override
    public void sell(String name, int price, int quantity) throws StockDoesNotExistException, StockNotAvailableException {
        // Creating fruit object which is to be sold and sell it using strategy
        int profitForCurrentSell = strategy.sell(new Fruit(name, price, quantity), fruitStock);

        // Adding current profit into total profit
        profit += profitForCurrentSell;
    }

    @Override
    public int getProfit() {
        return profit;
    }
}
