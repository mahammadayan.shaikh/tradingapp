package com.repository;

import com.excepetions.StockDoesNotExistException;
import com.excepetions.StockNotAvailableException;

public interface TradingRepository {
    void buy(String name, int price, int quantity);

    void sell(String name, int price, int quantity) throws StockDoesNotExistException, StockNotAvailableException;

    int getProfit();
}
