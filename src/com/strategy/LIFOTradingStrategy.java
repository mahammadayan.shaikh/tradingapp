package com.strategy;

import com.excepetions.StockDoesNotExistException;
import com.excepetions.StockNotAvailableException;
import com.models.Fruit;

import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.stream.Collectors;

public class LIFOTradingStrategy implements TradingStrategy {
    @Override
    public void buy(Fruit fruit, Map<String, Queue> fruitStock) {
        // Get the fruit from the stock
        Queue<Fruit> fruits = fruitStock.get(fruit.getName());

        // Check If we have this fruit in stock or not
        if (fruits == null) {
            fruits = new LinkedList<>();
        }

        // Adding this fruit in stock
        fruits.add(fruit);

        // Updating the Map
        fruitStock.put(fruit.getName(), fruits);
    }

    @Override
    public int sell(Fruit fruit, Map<String, Queue> fruitStock) throws StockDoesNotExistException, StockNotAvailableException {
        int profit = 0;
        int required_quantity = fruit.getQuantity();
        // Get the fruit from the stock
        Queue<Fruit> fruits = fruitStock.get(fruit.getName());

        // Check If we have this fruit in stock or not
        if (fruits == null) {
            // Throw Exception regarding unavailability of stock.
            throw new StockDoesNotExistException(fruit.getName()+" is not available in stock");
        }

        // Check if we have enough quantity of fruit in stock
        int available_quantity = fruits.stream().collect(Collectors.summingInt(Fruit::getQuantity));
        if (available_quantity < required_quantity) {
            // Throw Exception regarding unavailability of enough quantity
            throw new StockNotAvailableException(fruit.getName()+" is under stock");
        }

        // Get Fruits from stock in LIFO manner.
        while (required_quantity > 0) {
            // Get the fruit which was bought first
            Fruit curFruit = fruits.peek();
            if (required_quantity >= curFruit.getQuantity()) {
                // Get the entire stock of fruit as we need more quantities
                required_quantity -= curFruit.getQuantity();
                fruits.remove();

                //Update the profit
                profit += ((fruit.getPrice() - curFruit.getPrice()) * curFruit.getQuantity());
            } else {
                // Decrease the quantity as we have used some quantity of it
                curFruit.setQuantity(curFruit.getQuantity() - required_quantity);

                // Update the profit
                profit += ((fruit.getPrice() - curFruit.getPrice()) * required_quantity);

                // Set quantity to zero as we have got the desired quantities
                required_quantity = 0;
            }
        }
        fruitStock.put(fruit.getName(), fruits);
        return profit;
    }
}
