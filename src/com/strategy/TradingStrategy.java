package com.strategy;

import com.excepetions.StockDoesNotExistException;
import com.excepetions.StockNotAvailableException;
import com.models.Fruit;

import java.util.Map;
import java.util.Queue;

public interface TradingStrategy {
    void buy(Fruit fruit, Map<String, Queue> fruitStock);

    int sell(Fruit fruit, Map<String, Queue> fruitStock) throws StockDoesNotExistException, StockNotAvailableException;
}
