package com;

import com.enums.Command;
import com.excepetions.StockDoesNotExistException;
import com.excepetions.StockNotAvailableException;
import com.repository.TradingRepository;
import com.repository.TradingRepositoryImplementation;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // Repository object to perform trading operation
        TradingRepository repository = new TradingRepositoryImplementation();

        // Scanner to take input from Stdin
        Scanner scanner = new Scanner(System.in);

        // Variables to store fruit details
        String fruitName;
        int fruitPrice, fruitQty;

        try {
            while (true) {
                // Take Input from User
                String input = scanner.nextLine();

                // Split the line to get separate input
                String[] commands = input.split("\\s+");

                // Get the Name of command (BUY/SELL/PROFIT)
                Command command = Command.valueOf(commands[0]);

                // Apply corresponding action based on command
                switch (command) {
                    case BUY:
                        fruitName = commands[1];
                        fruitPrice = Integer.parseInt(commands[2]);
                        fruitQty = Integer.parseInt(commands[3]);
                        repository.buy(fruitName, fruitPrice, fruitQty);

                        // Printing The Success Message
                        System.out.println(String.format("BOUGHT %d KG %s AT %d RUPEES/KG", fruitQty, fruitName, fruitPrice));
                        break;
                    case SELL:
                        fruitName = commands[1];
                        fruitPrice = Integer.parseInt(commands[2]);
                        fruitQty = Integer.parseInt(commands[3]);
                        try {
                            repository.sell(fruitName, fruitPrice, fruitQty);
                            // Printing The Success Message
                            System.out.println(String.format("SOLD %d KG %s AT %d RUPEES/KG", fruitQty, fruitName, fruitPrice));
                        } catch (StockNotAvailableException notAvailableException) {
                            // Showing Message regarding unavailability of enough quantity
                            System.out.println(String.format("SORRY, %s IS UNDER STOCK", fruitName));

                        } catch (StockDoesNotExistException notExistException) {
                            // Showing Message regarding unavailability of stock.
                            System.out.println(String.format("SORRY, %s IS NOT AVAILABLE IN STOCK.", fruitName));
                        }
                        break;
                    case PROFIT:
                        System.out.println("" + repository.getProfit());
                        break;
                    case EXIT:
                        return;
                }
            }
        } catch (Exception e) {
            System.out.println("OOPS, SOMETHING WENT WRONG!!!");
        }
    }
}
