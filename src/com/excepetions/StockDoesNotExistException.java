package com.excepetions;

public class StockDoesNotExistException extends  Exception{
    public StockDoesNotExistException(String message) {
        super(message);
    }
}
